import clasify as cl
from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin

app = Flask(__name__)
app.debug = True


@app.route('/', methods=['POST'])
@cross_origin()
def index():
    if request.method == 'POST':
        data = request.json
        response = cl.response(data['question'])
        return jsonify(response)