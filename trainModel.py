# Vsetky kniznice treba doinstalovat
import json
import random
import nltk
import numpy as np
import tensorflow as tf
import tflearn
import pickle
# from nltk.stem.lancaster import LancasterStemmer
import stemmer

# stemmer = LancasterStemmer()


with open('intents.json') as json_data:
    intents = json.load(json_data)
# print(intents)
words = []
classes = []
documents = []
ignore_words = []
# Prejdenie a zaradnie slov do tried
for intent in intents['intents']:
    for pattern in intent['patterns']:
        # tokenizovanie slov
        w = nltk.word_tokenize(pattern)
        # pridanie do listu slov
        words.extend(w)
        # pridanie dokumentu do skorpusu
        documents.append((w, intent['tag']))
        # pridanie triedy
        if intent['tag'] not in classes:
            classes.append(intent['tag'])
# odstranenie duplicit
words = [stemmer.stem(w.lower()) for w in words if w not in ignore_words]
words = sorted(list(set(words)))

# odstranenie duplicitnych tried
classes = sorted(list(set(classes)))

print(len(documents), 'documents')
print(len(classes), 'classes')
print(len(words), 'words')

# vytvorenie testovacich dat
training = []
output = []

# vytvorenie prazdneho pola pre vystup
output_empty = [0] * len(classes)

# trenovaci set, vak slov, foreach cyklus
for doc in documents:
#     inicializovanie vaku slov
    bag = []
#   list tokenizovanych slov
    pattern_words = doc[0]
#   stem each slov
    pattern_words = [stemmer.stem(word.lower()) for word in pattern_words]
#   vytvorenie pola slov
    for w in words:
        bag.append(1) if w in pattern_words else bag.append(0)

#   Vysledkom je 0 alebo 1 ak tag sa nachadza v aktualnom tagu
    output_row = list(output_empty)
    output_row[classes.index(doc[1])] = 1


    training.append([bag, output_row])
    output.append(output_row)

# premiesanie dat
random.shuffle(training)
training = np.array(training)
output = np.array(output)

# vytvorenie trenovacej a testovacej skupiny
train_x = list(training[:,0])
train_y = list(training[:,1])

print(train_x[0])
print('')
print(train_y[0])
print("Velkost")
print(len(output[0]))
# resetovanie dat v grafe
# reset underlying graph data
# tf.reset_default_graph()
tf.compat.v1.reset_default_graph()
# Build neural network
net = tflearn.input_data(shape=[None, len(train_x[0])])
net = tflearn.fully_connected(net, 8)
net = tflearn.fully_connected(net, 8)
net = tflearn.fully_connected(net, len(output[0]), activation='softmax')
net = tflearn.regression(net)

# Define model and setup tensorboard
model = tflearn.DNN(net, tensorboard_dir='tflearn_logs')
# Start training (apply gradient descent algorithm)
model.fit(train_x, train_y, n_epoch=1000, batch_size=8, show_metric=True)
model.save('model.tflearn')
pickle.dump( {'words':words, 'classes':classes, 'train_x':train_x, 'train_y':train_y}, open( "training_data", "wb" ) )

